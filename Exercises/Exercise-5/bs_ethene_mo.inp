&GLOBAL
  PROJECT bs_ethene
  RUN_TYPE ENERGY
  PRINT_LEVEL LOW
&END GLOBAL

&FORCE_EVAL
  METHOD Quickstep              ! Electronic structure method
  &DFT
  BASIS_SET_FILE_NAME EMSL_BASIS_SETS
    &PRINT
      &MO_CUBES                 ! MOs to be written to cube-files.
        NHOMO 5
        NLUMO 5
      &END MO_CUBES
    &END PRINT
    &POISSON             
      PERIODIC NONE
      PSOLVER  WAVELET   
    &END POISSON
    &QS                         ! Parameters needed to set up the Quickstep framework
      METHOD GAPW               ! Method: PM6 semiempirical method 
    &END QS

    &SCF                        ! Convergence criteria for self consistent problem      
      EPS_SCF 1.0E-6
      SCF_GUESS ATOMIC
      MAX_SCF 150
      &OUTER_SCF
        EPS_SCF 1.0E-6
        MAX_SCF 100
      &END
    &END SCF

    &XC                        ! Parameters for exchange potential 
      &XC_FUNCTIONAL NONE   
      &END XC_FUNCTIONAL
      &HF                      ! Hartree Fock exchange.   
        &SCREENING             ! Setting screening of the electronic repulsion              
        &END SCREENING
      &END HF
    &END XC
  &END DFT

 &SUBSYS
    &CELL
      ABC 10 10 10
      PERIODIC NONE              ! Non periodic calculations. That's why the POISSON scetion is needed 
    &END CELL
    &TOPOLOGY                    ! Section used to center the atomic coordinates in the given box. Useful for big molecules
      &CENTER_COORDINATES
      &END
    &END
    &COORD
    C         -2.15324        3.98235        0.00126
    C         -0.83403        4.16252       -0.00140
    H         -0.25355        3.95641        0.89185
    H         -0.33362        4.51626       -0.89682
    H         -2.65364        3.62861        0.89669
    H         -2.73371        4.18846       -0.89198
    &END COORD
    &KIND H                    ! Basis set and potential for H
     BASIS_SET aug-cc-pVTZ
     POTENTIAL ALL
    &END KIND
    &KIND C                    ! Basis set and potential for C
     BASIS_SET aug-cc-pVTZ
     POTENTIAL ALL
    &END KIND
  &END SUBSYS
&END FORCE_EVAL
