
#remove the file if it exist
if [ -f trajectory3.xyz ]
then
   rm trajectory3.xyz
fi

#create trajectory taking the final position of aech replica
for i in `seq 1 8`
   do tail -n 40 output_neb3-pos-Replica_nr_${i}-1.xyz >> trajectory3.xyz
done

#create energy file
grep E trajectory3.xyz | awk '{print NR, $6}' > energy3.dat
